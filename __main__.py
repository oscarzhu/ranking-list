# -*- coding: UTF-8 -*-
from cmath import inf
from flask import *
import hashlib
from datetime import datetime
import sqlite3

run_cnt=1

def edit_sql():
    conn = sqlite3.connect('./SQL.sqlite3')
    cur = conn.cursor()
    global list_
    for i in list_:
        cur.execute("UPDATE RANKING SET POINT = " +
                    str(i[1])+" WHERE NAME = '"+i[0]+"';")
        cur.execute("UPDATE RANKING SET DESIGNATION = '" +
                    str(i[2])+"' WHERE NAME = '"+i[0]+"';")
    conn.commit()
    conn.close()


def view_sql():
    conn = sqlite3.connect('./SQL.sqlite3')
    cur = conn.cursor()
    global list_, run_cnt
    for i in range(len(list_)):
        list_[i][1] = list(*cur.execute(
            "SELECT POINT FROM RANKING WHERE NAME = '"+list_[i][0]+"';"))
        list_[i][1]=list_[i][1][0]
        list_[i][2] = list(*cur.execute(
            "SELECT DESIGNATION FROM RANKING WHERE NAME = '"+list_[i][0]+"';"))
        list_[i][2]=list_[i][2][0]
    if run_cnt:
        run_cnt = 0
        sort_()
    conn.close()


def sha256(a):
    sha256 = hashlib.sha256()
    sha256.update(a.encode('utf-8'))
    return sha256.hexdigest()


def integer(num):
    try:
        int(num)
        return True
    except ValueError:
        flash("必须输入整数字(不能使用科学计数法)!")
        return False


def sort_():
    global list_
    list__ = []
    n = len(list_)
    for i in range(n):
        max = -inf
        index = None
        for j in range(n):
            if list_[j][1] > max:
                index = j
                max = list_[j][1]
        list__.append(list_[index].copy())
        list_[index][1] = -inf
    list_ = list__[:n]


def designation_():
    global list_
    for i in range(len(list_)):
        if list_[i][1] <= 0:
            list_[i][2] = "菜鸟"
        elif 0 < list_[i][1] < 50:
            list_[i][2] = "平民"
        elif 50 <= list_[i][1] < 100:
            list_[i][2] = "高手"
        else:
            list_[i][2] = "精英"


app = Flask(__name__)


app.secret_key = 'admin login key'

list_ = [["1", 0, "菜鸟"], ["2", 0, "菜鸟"], ["3", 0, "菜鸟"], ["4", 0, "菜鸟"], ["5", 0, "菜鸟"], [
    "6", 0, "菜鸟"], ["7", 0, "菜鸟"], ["8", 0, "菜鸟"], ["9", 0, "菜鸟"], ["10", 0, "菜鸟"]]


@app.route("/")
def index():
    view_sql()
    return render_template('index.html', list=list_)


@app.route("/admin/", methods=['GET', 'post'])
def admin():
    view_sql()
    if session.get("login") != "1":
        print(str(datetime.now()), "用户访问admin时未登录,已经重定向到login.")
        return redirect("/login")
    global list_
    if request.method == 'GET':
        return render_template('admin.html', list=list_)
    t1 = request.form.get("t1")

    if integer(t1):
        if int(request.form.get("for", -1)) >= 0:
            list_[int(request.form.get("for"))][1] += int(t1)
            designation_()
            print(str(datetime.now()), "管理员向", list_[int(request.form.get("for"))][0], "增加了", t1, "分。\n"+list_[int(
                request.form.get("for"))][2], list_[int(request.form.get("for"))][0], "当前有", str(list_[int(request.form.get("for"))][1]), "分.\n")
            sort_()
            edit_sql()
    return render_template('admin.html', list=list_)


@app.route("/login/", methods=['GET', 'post'])
def login():
    if session.get("login") == "1":
        print(str(datetime.now()), "用户访问login时已登录,已经重定向到admin.")
        return redirect("/admin")
    if request.method == 'GET':
        session.get('login', "0")
        flash("请先登录")
        return render_template("login.html")
    username = request.form.get("username")
    password = request.form.get("password")
    if sha256(username) == "a7f4beaa07413755daf20131580496d25ba9fe01a026c4a2199a9cb373f25a06" and sha256(password) == "11a3bb57290172d41c5261c94a9495b9e5c2048833f7c8f985e2594ecfaca5fa":
        session["login"] = "1"
        print(str(datetime.now()), "管理员已登录")
        return redirect("/admin")
    else:
        flash("用户名或密码错误！")
        print(str(datetime.now()), "用户输入管理员账号/密码错误")
        return render_template("login.html")


if __name__ == '__main__':
    app.run('0.0.0.0', 5932)
